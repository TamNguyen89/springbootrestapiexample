package com.tam.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tamnguyen on 25/02/2017.
 */

@SpringBootApplication(scanBasePackages = {"com.tam.springboot"})
public class SpringBootRestApiApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestApiApp.class, args);
    }
}
