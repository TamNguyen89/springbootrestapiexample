package com.tam.springboot.service;

import com.tam.springboot.model.User;

import java.util.List;

/**
 * Created by tamnguyen on 25/02/2017.
 */
public interface UserService {

    User findById(long id);

    User findByName(String name);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    List<User> findAllUsers();

    void deleteAllUsers();

    public boolean isUserExist(User user);
}
